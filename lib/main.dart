
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myfirstproject/screen/slpash_screen.dart';
import 'package:myfirstproject/send_otp/send_otp_screen.dart';

import 'base/base_screen.dart';
import 'contansts/screen_util.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
   //await FirebaseAuth.instance.useEmulator('http://localhost:9099');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return MaterialApp(
      title: 'My Firebase Project',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.

        primaryColor: Colors.black,
      ),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        ScreenUtil.BASE_SCREEN: (BuildContext context) => new SendOtpScreen(),
      },
    );
  }
}
