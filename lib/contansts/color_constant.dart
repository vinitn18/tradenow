import 'dart:ui';

class ColorUtil {
  static const Color backgroundColor = const Color(0xffF4F5F9);
  static const Color titleColor = const Color(0xff454F63);
  static const Color colorBlack1 = const Color(0xff231F20);
  static const Color colorGrey1 = const Color(0xff868E96);
  static const Color colorPrimary = const Color(0xff00A0DF);
  static const Color colorRed = const Color(0xffff0000);
  static const Color colorOrange = const Color(0xffFF9011);
  static const Color colorGreen = const Color(0xff0CC443);

  static const Color btnStartColor = const Color(0xFF69C7CF);
  static const Color btnEndColor = const Color(0xFF00A0DF);
  static const Color colorInputLabel = const Color(0xFF454F63);
  static const Color colorInputBorder = const Color(0xFFDEDFE2);

  static const Color colorInputText = const Color(0xff454F63);
  static const Color colorInputHint = const Color(0xffA2A5B5);
  static const Color colorPreloginTitle = const Color(0xFF171717);
  static const Color colorPreloginInputHint = const Color(0xFF8F92A1);

  static const Color colorEmployeeListName = const Color(0xff454F63);
  static const Color colorEmployeeListDes = const Color(0xFF78849E);

  static const Color colorNotificationMsg = const Color(0xFF78849E);
}
