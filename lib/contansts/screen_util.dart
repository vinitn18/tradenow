class ScreenUtil {
  static String SPLASH_SCREEN = "/SplashScreen";
  static String OTP_SCREEN = "/OtpScreen";
  static String HOME_SCREEN = "/HomeScreen";
  static String BASE_SCREEN = "/BaseScreen";
  static String OTP_VERIFICATION_SCREEN = "/OtpVerification";

}