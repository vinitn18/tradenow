class ConstantUtil {
  //FIREBASE
  static String FIREBASE_TABLE_USERS = "users";
  static String FIREBASE_TABLE_KINS_CONTACT = "kinsContacts";

  static String FIREBASE_TABLE_EMPLOYEES = "employees";
  static String FIREBASE_TABLE_COLLEAGUE_CONTACT = "colleagueContacts";

  static String FIREBASE_KEY_USER_UUID = "userId";
  static String FIREBASE_KEY_CREATED_AT = "createdAt";
  static String FIREBASE_KEY_UPDATED_AT = "updatedAt";

  static String FIREBASE_KEY_PHONE = "phone";
  static String FIREBASE_KEY_FNAME = "fname";
  static String FIREBASE_KEY_FULL_NAME = "fullName";
  static String FIREBASE_KEY_RELATION = "relation";


  static String FIREBASE_KEY_MNAME = "mname";
  static String FIREBASE_KEY_LNAME = "lname";
  static String FIREBASE_KEY_GENDER = "gender";
  static String FIREBASE_KEY_MARIAL_STATUS = "marialStatus";
  static String FIREBASE_KEY_DOB = "dob";
  static String FIREBASE_KEY_TOTAL_CHILDREN = "totalChildren";
  static String FIREBASE_KEY_EDUCATION = "education";
  static String FIREBASE_KEY_EMAIL = "email";
  static String FIREBASE_KEY_W_ID = "whatsAppId";
  static String FIREBASE_KEY_CITY = "city";
  static String FIREBASE_KEY_STATE = "state";
  static String FIREBASE_KEY_RESIDENCE_TIME = "residenceTime";

  static String FIREBASE_KEY_POS_ID_PHOTO = "positiveIdPhoto";
  static String FIREBASE_KEY_HOL_ID_PHOTO = "holdingIdPhoto";
  static String FIREBASE_KEY_PROFILE_PHOTO = "profilePhoto";


  //EMPLOYMENT DETAILS
  static String FIREBASE_KEY_COM_NAME = "companyName";
  static String FIREBASE_KEY_OCCUPATION = "occupation";
  static String FIREBASE_KEY_COMPANY_STATE = "state";
  static String FIREBASE_KEY_COMPANY_CITY = "city";
  static String FIREBASE_KEY_COMPANY_ADDRESS = "address";
  static String FIREBASE_KEY_MONTHLY_INCOME = "monthlyIncome";
  static String FIREBASE_KEY_SALARY_PAY_DATE = "salaryPayDate";
  static String FIREBASE_KEY_COMPANY_CONTACT = "contact";
  static String FIREBASE_KEY_COMPANY_EMAIL = "email";
  static String FIREBASE_KEY_COMPANY_EMP_TIME = "employmentTime";
  static String FIREBASE_KEY_WORK_PHOTO1 = "workPhoto1";
  static String FIREBASE_KEY_WORK_PHOTO2 = "workPhoto2";



  //FIREBASE ERROR CODES
  static String FIREBASE_ERR_CODE_INVALID = "invalid-phone-number";

  static String PREF_ISLOGIN = "PREF_ISLOGIN";
  static String PREF_LOGIN_USER = "PREF_LOGIN_USER";
  static String PREF_LOGIN_USER_UUID = "PREF_LOGIN_USER_UUID";
  static String PREF_LOGIN_USER_PHONE = "PREF_LOGIN_USER_PHONE";



}
