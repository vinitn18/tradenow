class StringUtil {
/*
  static final String cyrrency_symbol = "₦";
  static final String home_max_loan_amount_text = "Max loan amount";
  static final String home_max_loan_amount = "5,000,000";
  static final String home_max_loan_period_text = "Longest period";
  static final String home_max_loan_period = "120 Days";
  static final String home_max_loan_interest_text = "*Low to 5% per day";

  static final String Our_Advantage_text = "Our Advantage";
  static final String your_max_loan_amount_text = "Your Maximum Loan Amount";
  static final String your_longest_period_text = "Your Longest Period(days)";
  static final String your_max_loan_amount = "4,000.00";
  static final String your_longest_period = "6";

*/

  static final String ERR_PHONE_INVALID = "The provided phone number is not valid.";
  static final String SUCC_OTP_SENT = "OTP successfully sent.";
  static final String ERR_OTP_TIMEOUT = "OTP time out.";

 /* static const ERR_MISSING_FNAME = "Please enter first name.";
  static const ERR_MISSING_MNAME = "Please enter middle name.";
  static const ERR_MISSING_LNAME = "Please enter last name.";
  static const ERR_MISSING_PHONE = "Please enter phone.";
  static const ERR_MISSING_EMAIL = "Please enter email.";
  static const ERR_MISSING_GENDER = "Please select gender.";
  static const ERR_MISSING_MSTATUS = "Please select marital status.";
  static const ERR_MISSING_STATE = "Please select state.";
  static const ERR_MISSING_CITY = "Please select city.";
  static const ERR_MISSING_DOB= "Please select date of birth.";
  static const ERR_MISSING_ADDRESS = "Please enter address.";
  static const ERR_MISSING_CONTACT_TOTAL = "Please select 3 contact.";
  static const ERR_MISSING_CONTACT = "Please select contact.";
  static const ERR_MISSING_PHOTO = "Please upload photo.";
  static const ERR_MISSING_HOLDING_ID = "Please upload holding id photo.";
  static const ERR_MISSING_POSITIVE_ID = "Please upload positive id photo.";



  static const ERR_MISSING_SNAME = "Please enter site name.";
  static const ERR_MISSING_SLOCATION = "Please enter site location.";
  static const ERR_MISSING_SDESCRIPTION = "Please enter site description.";
  static const ERR_MISSING_WROKSTATUS = "Please select work status.";


  //PERSONAL INFO
  static final String SUCC_PROFILE_UPDATED = "Profile updated successfully.";


  static const ERR_MISSING_NAME = "Please enter name.";
  static const ERR_MISSING_BADDRESS = "Please enter address.";
  static const ERR_MISSING_BNAME = "Please select bank name.";
  static const ERR_MISSING_CNAME = "Please enter company name.";
  static const ERR_MISSING_OCCUPTION = "Please enter occuption/sector name.";
  static const ERR_MISSING_MONTHLYINCOME = "Please select monthly income.";
  static const ERR_MISSING_SALARYPAYDATE = "Please select salary per day.";
  static const ERR_MISSING_EMPTIME = "Please select employee time.";
*/
}
