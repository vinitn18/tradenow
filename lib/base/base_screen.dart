import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseScreen extends StatefulWidget {
  @override
  BaseScreenState createState() => BaseScreenState();
}

class BaseScreenState extends State<BaseScreen> {

  late SharedPreferences prefs;
  String photo = "", name = "", phone = "";
 // List<DrawerItem> menuList = [];

/*  int selectedPos = 0;
  PackageInfo packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    init();
  }

  init() async {
  //  menuList = DrawerMenuUtil.getDrawerMenu();
   // packageInfo = await PackageInfo.fromPlatform();
    prefs = await SharedPreferences.getInstance();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: new Drawer(
            child: Container(
              decoration: new BoxDecoration(
                // color: Colors.purple,
                gradient: new LinearGradient(
                  colors: [Colors.black, Colors.grey],
                ),
                borderRadius:
                BorderRadius.only(bottomRight: Radius.circular(200.0)),
              ),
             /* child: Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 50.0, 50.0, 50.0),
                child: Column(
                  children: [
                   // getDrawerHeader(),
                    Expanded(child: getDrawerItems()),
                   // getFooter()
                  ],
                ),
              ),*/
            )),
      //  body: getScreen(selectedPos)
    );
  }

  getScreen(int pos) {
    switch (pos) {
      case 0:
        //return HomeScreen();
      case 1:
       // return MyDetailsScreen();
      case 2:
      //  return LoanDetailScreen();
      case 3:
      //  return HomeScreen();
    }
  }

  /*showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Column(
            children: [
              Image.asset(
                'assets/images/alert_icon.png',
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text('Success',
                    textAlign: TextAlign.center, style: styleDialogText),
              ),
            ],
          ),
          content: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(text: 'Are you want to ', style: styleDialogMsg),
                TextSpan(text: 'logout?', style: styleDialogMsg2),
              ],
            ),
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Container(
              height: 0.3,
              color: const Color(0xFF707070),
              child: SizedBox(
                width: 500,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'NO',
                      textAlign: TextAlign.center,
                      style: styleDialogOk,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                  width: 120,
                ),
                Container(
                  height: 50,
                  color: const Color(0xFF707070),
                  child: SizedBox(
                    width: 0.5,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                      logOut();
                    },
                    child: Text(
                      'YES',
                      textAlign: TextAlign.center,
                      style: styleDialogOk,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                  width: 120,
                ),
              ],
            ),
          ],
        );
      },
    );
  }*/

  /*getDrawerHeader() {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
      ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: photo.isNotEmpty
            ? Image.network(
          photo,
          height: 80.0,
          width: 80.0,
          fit: BoxFit.cover,
        )
            : Image.asset(
          'assets/images/side_menu_placeholder_img.png',
          height: 80.0,
          width: 80.0,
          fit: BoxFit.cover,
        ),
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(name.isNotEmpty ? name : "", style: styleDrawerUserName),
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 10, 0, 0),
              child: Text(phone, style: styleDrawerUserPhone),
            ),
          ],
        ),
      )
    ]);
  }*/

  getDrawerItems() {
    /*return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: menuList.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final item = menuList[index];

        return InkWell(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Image.asset(
                    selectedPos == index ? item.iconDown : item.icon,
                    // height: 88,
                    // width: 88,
                  ),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                      child: Text(item.name,
                          style: TextStyle(
                              color: selectedPos == index
                                  ? Colors.white
                                  : Colors.white.withOpacity(0.6),
                              fontSize: 16,
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.w500)))
                ],
              ),
              Container(
                margin: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                width: double.infinity,
                height: 0.2,
                color: Colors.white,
              ),
            ],
          ),
          onTap: () {
            Navigator.pop(context);
            selectedPos = index;

            if (selectedPos == 6) {
              showAlert(context);
            } else
              setState(() {});
          },
        );
      },
    );*/
  }

  /*getFooter() {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
        child: Text("Version ${packageInfo.version}",
            style: TextStyle(
                color: Colors.white.withOpacity(0.6),
                fontSize: 11,
                fontFamily: "Montserrat",
                fontWeight: FontWeight.w700)),
      )
    ]);
  }*/

  /*logOut() {
    FirebaseAuth.instance.signOut();
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => SendOtpScreen(),
      ),
          (route) => false,
    );
    prefs.clear();
  }*/
}
