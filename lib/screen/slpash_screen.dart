import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:myfirstproject/base/base_screen.dart';
import 'package:myfirstproject/send_otp/send_otp_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  late SharedPreferences prefs;

  FirebaseAuth auth = FirebaseAuth.instance;

  var logger = Logger();

  @override
  void initState() {
    super.initState();

    init();
  }

  Widget getNotificationWidget() {
    return Container();
  }

  init() async {
    prefs = await SharedPreferences.getInstance();

    if (auth.currentUser == null)
      Timer(
          Duration(seconds: 3),
          () => {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => SendOtpScreen()))
              });
    else
      Timer(
          Duration(seconds: 3),
          () => {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => SendOtpScreen()))
              });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black87,
      child: Center(
        child: Icon(
          Icons.person,
          size: 35,
          color: Colors.grey,
        ),
      ),
    );
  }
}
