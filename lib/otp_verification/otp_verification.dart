import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:myfirstproject/contansts/color_constant.dart';
import 'package:myfirstproject/contansts/constant.dart';
import 'package:myfirstproject/contansts/screen_util.dart';
import 'package:myfirstproject/contansts/string.dart';
import 'package:myfirstproject/home/home_sceen.dart';
import 'package:myfirstproject/util/platform_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms_autofill/sms_autofill.dart';

class OtpVerification extends StatefulWidget {
  OtpVerification(
      {required this.phone,
      required this.verificationId,
      int? this.resendToken});

  final String phone, verificationId;
  final int? resendToken;

  @override
  OtpVerificationState createState() => OtpVerificationState();
}

class OtpVerificationState extends State<OtpVerification> {
  var otpCode = "";
  var logger = Logger();
  late SharedPreferences prefs;
  String verificationId = "";
  int? resendToken;
  bool isApiCalled = false;

  FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  void initState() {
    verificationId = widget.verificationId;
    resendToken = widget.resendToken;

    // TODO: implement initState
    super.initState();
    listenAutoSms();
  }

  listenAutoSms() async {
    final signature = await SmsAutoFill().getAppSignature;
    if (!PlatformInfo().isWeb()) await SmsAutoFill().listenForCode;
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    SmsAutoFill().unregisterListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: 200,
                width: double.infinity,
                decoration: new BoxDecoration(
                  // color: Colors.purple,
                  gradient: new LinearGradient(
                    colors: [Colors.black87, Colors.grey],
                  ),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(100.0)),
                ),
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 40, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: new GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios_outlined,
                            size: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35),
                        child: Text(
                          'OTP Authentication',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 12),
                        child: Text(
                          'An Authentication code has been sent to ${widget.phone}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 100, left: 20, right: 20),
                  child: PinFieldAutoFill(
                      //controller: _otpController,
                      decoration: UnderlineDecoration(
                        colorBuilder: FixedColorBuilder(CupertinoColors.black),
                      ),
                      //ration, BoxLooseDecoration or BoxTightDecoration see https://github.com/TinoGuo/pin_input_text_field for more info,
                      currentCode: otpCode,
                      // prefill with a code
                      onCodeSubmitted: (code) {
                        signInUser(code);
                      },
                      //code submitted callback
                      onCodeChanged: (code) {
                        otpCode = code!;
                        // //logger.e("OTP is ${code}");
                        if (code.length == 6) {
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                        setState(() {});
                      },
                      //code changed callback
                      codeLength: 6 //code length, default 6
                      )),
              Container(
                margin: EdgeInsets.fromLTRB(20, 80, 20, 0),
                height: 40,
                child: new Material(
                  child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => HomeScreen()));
                      /* if (otpCode.length == 6) {
                        showLoader(true);
                        signInUser(otpCode);
                      }*/
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 32.0),
                              child: Text(
                                "Verify",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Image.asset(
                            'assets/images/next_white_arrow.png',
                            width: 32,
                          ),
                        ],
                      ),
                    ),
                  ),
                  color: Colors.transparent,
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    gradient: LinearGradient(
                      end: Alignment.bottomLeft,
                      colors: (otpCode.length == 6
                          ? [
                              Colors.black87,
                              Colors.grey,
                            ]
                          : [
                              Colors.grey.withOpacity(0.5),
                              Colors.grey.withOpacity(0.5),
                            ]),
                    )),
              ),
              GestureDetector(
                  onTap: () {
                    showLoader(true);
                    sendOtp();
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 25),
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Resend OTP',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 14,
                          fontFamily: 'Montserrat',
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ))
            ],
          ),
          Center(
            child: Visibility(
              visible: isApiCalled,
              child: Image.asset(
                'assets/images/launcher.png',
                height: 70,
                width: 70,
              ),
            ),
          ),
        ],
      ),
    );
  }

  showLoader(bool isShow) {
    setState(() {
      isApiCalled = isShow;
    });
  }

  signInUser(String code) async {
    // Create a PhoneAuthCredential with the code
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: code);

    // Sign the user in (or link) with the credential
    auth
        .signInWithCredential(credential)
        .then((value) => {
              logger.e("Logged in user uuid ${value.user!.uid}"),
              addUserData(value.user!.uid),
            })
        .catchError(
            (onError) => {showLoader(false), showToast(onError.toString())});
  }

  /**
   * UPDATE LOGGED IN USER DATA IN FIRE STORE
   */
  addUserData(String uuid) {
    Map<String, dynamic> userData = {
      ConstantUtil.FIREBASE_KEY_PHONE: widget.phone,
      ConstantUtil.FIREBASE_KEY_USER_UUID: uuid,
      ConstantUtil.FIREBASE_KEY_CREATED_AT: FieldValue.serverTimestamp()
    };

    users
        .doc(uuid)
        .set(userData)
        .then((value) => {
              prefs.setString(ConstantUtil.PREF_LOGIN_USER_UUID, uuid),
              prefs.setString(ConstantUtil.PREF_LOGIN_USER_PHONE, widget.phone),
              showLoader(false),
              navigateToHome()
            })
        .catchError((onError) => {
              logger.e(onError),
              showLoader(false),
              showToast(onError.toString())
            });
  }

  showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: ColorUtil.colorPrimary,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  sendOtp() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: widget.phone,
      timeout: const Duration(seconds: 120),
      forceResendingToken: widget.resendToken,
      verificationCompleted: (PhoneAuthCredential credential) {
        auth
            .signInWithCredential(credential)
            .then((value) => {
                  logger.e("sendOtp Logged in user uuid ${value.user!.uid}"),
                  addUserData(value.user!.uid),
                })
            .catchError((onError) => {showToast(onError.toString())});
      },
      verificationFailed: (FirebaseAuthException e) {
        showLoader(false);
        if (e.code == ConstantUtil.FIREBASE_ERR_CODE_INVALID) {
          showToast(StringUtil.ERR_PHONE_INVALID);
        } else
          showToast(e.message!);
      },
      codeSent: (String verificationId, int? resendToken) {
        showLoader(false);
        this.verificationId = verificationId;
        this.resendToken = resendToken;
        showToast(StringUtil.SUCC_OTP_SENT);
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        showLoader(false);
        showToast(StringUtil.ERR_OTP_TIMEOUT);
      },
    );
  }

  navigateToHome() {
    Navigator.pushNamedAndRemoveUntil(
        context, ScreenUtil.HOME_SCREEN, (r) => false);
  }
}
