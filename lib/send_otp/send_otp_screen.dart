import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:myfirstproject/contansts/screen_util.dart';
import 'package:myfirstproject/otp_verification/otp_verification.dart';

class SendOtpScreen extends StatefulWidget {
  @override
  SendOtpScreenState createState() => SendOtpScreenState();
}

class SendOtpScreenState extends State<SendOtpScreen> {
  final _phoneController = TextEditingController();
  bool isApiCalled = false;

  // final countryCode = "+234";
  final countryCode = "+91";

  var logger = Logger();

  FirebaseAuth auth = FirebaseAuth.instance;

  Future registerUser(
      String phone, Duration duration, BuildContext context) async {}
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: 200,
                width: double.infinity,
                decoration: new BoxDecoration(
                  // color: Colors.purple,
                  gradient: new LinearGradient(
                    colors: [Colors.black87, Colors.grey],
                  ),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(100.0)),
                ),
                child: Container(
                  margin: EdgeInsets.fromLTRB(20.0, 100, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sign In',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 12),
                        child: Text(
                          'Welcome back',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 80, 0, 0),
                      child: Text(
                        'Enter mobile number',
                      ),
                    ),
                    Container(
                        child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          child: Text(
                            countryCode,
                          ),
                        ),
                        Expanded(
                          child: TextField(
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(10)
                              ],
                              controller: _phoneController,
                              onChanged: (text) {
                                if (_phoneController.text.length == 10)
                                  FocusScope.of(context)
                                      .requestFocus(FocusNode());
                                setState(() {});
                              },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintText: "Enter mobile number",
                                border: InputBorder.none,
                              )),
                        ),
                      ],
                    )),
                    Container(
                        height: 0.5,
                        color: (_phoneController.text.isEmpty
                            ? Color(0xFF8F92A1)
                            : Color(0xFF00A0DF))),
                    new GestureDetector(
                        onTap: () {},
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 70, 0, 0),
                          height: 40,
                          child: new Material(
                            child: new InkWell(
                              onTap: () {
                                if (_phoneController.text.length == 10) {
                                  setState(() {
                                    isApiCalled = true;
                                  });
                                  sendOtp();
                                }
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20.0, 0, 20, 0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 32.0),
                                        child: Text(
                                          "Send OTP",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_right_alt_outlined,
                                      size: 35,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            color: Colors.transparent,
                          ),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(100)),
                              gradient: LinearGradient(
                                  end: Alignment.bottomLeft,
                                  colors: (_phoneController.text.length == 10
                                      ? [
                                          Colors.black87,
                                          Colors.grey,
                                        ]
                                      : [
                                          Colors.grey.withOpacity(0.5),
                                          Colors.grey.withOpacity(0.5),
                                        ]))),
                        ))
                  ],
                ),
              )
            ],
          ),
          Center(
            child: Visibility(
              visible: isApiCalled,
              child: Image.asset(
                'assets/images/launcher.png',
                height: 70,
                width: 70,
              ),
            ),
          ),
        ],
      ),
    );
  }

  sendOtp() async {
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => OtpVerification(
                phone: "${countryCode}${_phoneController.text}",
                verificationId: "",
                resendToken: 0)));
  }

  /*sendOtp() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: countryCode + _phoneController.text,
      timeout: const Duration(seconds: 120),
      verificationCompleted: (PhoneAuthCredential credential) {
        auth
            .signInWithCredential(credential)
            .then((value) => {
                  setState(() {
                    isApiCalled = false;
                  }),
                  navigateToHome()
                })
            .catchError((onError) => {showToast(onError.toString())});
      },
      verificationFailed: (FirebaseAuthException e) {
        setState(() {
          isApiCalled = false;
        });
        if (e.code == ConstantUtil.FIREBASE_ERR_CODE_INVALID) {
          showToast(StringUtil.ERR_PHONE_INVALID);
        } else
          showToast(e.message!);
      },
      codeSent: (String verificationId, int? resendToken) {
        setState(() {
          isApiCalled = false;
        });
        showToast(StringUtil.SUCC_OTP_SENT);

        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => OtpVerification(
                phone: "${countryCode}${_phoneController.text}",
                verificationId: verificationId,
                resendToken: resendToken),
          ),
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }*/

  showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  /**
   * NAVIGATE USER TO HOME SCREEN
   */
  navigateToHome() {
    Navigator.pushNamedAndRemoveUntil(
        context, ScreenUtil.BASE_SCREEN, (r) => false);
  }
}
